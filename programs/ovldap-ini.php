; <?php /*

; the fields should be modified in registry under the /ldap_generic/ folder following the same structure
; values must not be modified in this file because this will be overwritten with the next addon upgrade

[ldap]
ovidentia_parameters=1 		; Use ovidentia parameters instead of host, port, binddn, password, encoding
host=localhost
port=389
bindn="cantico@localhost"	; LDAP user dn used for connexion, must have access on users population read only
password="cantico"			; LDAP user password
encoding="utf-8" 			; LDAP directory content encoding : utf-8 | iso-8859-1 | t61

[general]
timer_active=0 				; enable or disable LibTimer synchronisation
timer_hour=23 				; allow synchronization from 23:00 to 24:00
logfile=/var/log/ovidentia/%s-ldap.log ; log file, %s will be replaced by the database name
password=ovidentia			; default password used in ovidentia database (this password is not used with ldap authentication)
is_confirmed=1				; status for new accounts
disableusers=0 				; disable ovidentia users not present in LDAP server
neverdisableadmin=0 		; Never disable ovidentia administrators
neverdisableauth=0	 		; Never disable users without LDAP authentication
defaultemail="register@cantico.fr"

[users]
ovidentia_basedn=1 			; user ovidentia basedn instead of the usersbasedn key
ovidentia_mapping=1 		; use ovidentia mapping instead of the [mapping] section
usersbasedn="CN=Users,DC=cantico,DC=fr"
filter="(|(objectclass=person))"

[exclude_users]
; attribute1="value1"
; attribute2="value2"


[mapping]
nickname="samaccountname"
lastname="sn"
firstname="givenname"
middlename=""
email="mail"

departmentnumber="department"
btel="telephonenumber"
mobile="mobile"
htel="homephone"
bfax="facsimiletelephonenumber"
title="title"
organisationname="company"
bstreetadress="streetadress"
bpostalcode="postalcode"
bstate="st"
bcountry="co"

; additionnal fields :
; babdirf27=""
; babdirf28=""


[groups]
; Attribut LDAP de l utilisateur qui contient le groupe a synchroniser
groupsattribute=""                ; champ de la fiche utilisateur contenant le nom du groupe
root=0                            ; id du groupe racine

[ldapgroups]

userfield="memberOf"		      ; champ de la fiche utilisateur contenant la liste des groupes, vider pour desactiver
root=0            			      ; id du groupe racine
create_groups=0				      ; 0: creation des des groupes desactivee 1: creation des groupes activee
remove_from_group=0			      ; user is removed from group if group not found in ldap group tree
remove_from_groups_on_disable=0   ;   
group_path_method=dn              ; possibles values are dn, memberof 
                                  ;   memberof : use the memberof attribute recusively on each group to get the full path
                                  ;   dn : use the full dn path to get the groups path, usefull if parents entities are not groups

[ldapoptions]
LDAP_OPT_PROTOCOL_VERSION=3
LDAP_OPT_REFERRALS=0


[orgchart]
rebuild_orgchart=0
rebuild_orgchart_basegroup=0
rebuild_orgchart_name="Import"
entity_field="Entite parent"
parent_entity_field="Service"
role_field="Titre"

[notifications]
recipients=""

; */ ?>
