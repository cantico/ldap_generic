<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

define("OVLDAP_INI_FILE", dirname(__FILE__)."/ovldap-ini.php");

/**
 * 
 * @param string $str
 * @return string
 */
function ovldap_translate($str)
{
	return bab_translate($str, 'ldap_generic');
}

/**
 * 
 * @return bab_Registry
 */
function ovldap_registry()
{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/ldap_generic/');
	
	return $registry;
}



/**
 * Get configuration from registry according to the ini strucuture
 * @return array
 */
function ovldap_getIniFile()
{
	$arr_ini = parse_ini_file(OVLDAP_INI_FILE, true);
	if( count($arr_ini) == 0 )
	{
		throw new Exception("Cannot read ini file ".OVLDAP_INI_FILE);
	}

	if( empty($arr_ini['general']['password']) )
	{
		throw new Exception("error", "Global password is empty");
	}

	return $arr_ini;
}


/**
 * Get configuration from registry according to the ini strucuture
 * @return array
 */
function ovldap_getConfiguration()
{
	static $arr_ini = null;
	
	if (!isset($arr_ini))
	{
		
		try {
			$arr_ini = ovldap_getIniFile();
		} catch (Exception $e)
		{
			ovldap_writeLog("error", $e->getMessage());
			ovldap_exit();
		}
	
		// search for over loaded  value in registry
	
		$registry = bab_getRegistryInstance();
	
		foreach($arr_ini as $section => $sectionconfig)
		{
			$registry->changeDirectory("/ldap_generic/$section");
			$I = $registry->getValueEx(array_keys($sectionconfig));
			
			/*@var $I bab_RegistryIterator */
			
			foreach($I as $arr)
			{
				$arr_ini[$section][$arr['key']] = $arr['value'];
			}
		}

	}
	
	return $arr_ini;
}


require_once $GLOBALS['babInstallPath'] . 'utilit/ocapi.php';

if (!function_exists('bab_OCGetOrgChartByName')) {

	function bab_OCGetOrgChartByName($name , $delegationId = 0)
	{
		global $babDB, $babBody;
	
		$name = trim($name);
		if (0 === mb_strlen($name)) {
			return null;
		}
	
		$sql = '
			SELECT id
			FROM ' . BAB_ORG_CHARTS_TBL . '
			WHERE
				id_dgowner = ' . $babDB->quote($delegationId) . '
			AND name LIKE ' . $babDB->quote($babDB->db_escape_like($name));
	
		//bab_debug($sQuery);
		$orgCharts = $babDB->db_query($sql);
		if (!$orgCharts) {
			return null;
		}
		if (false !== ($orgChart = $babDB->db_fetch_assoc($orgCharts))) {
			return $orgChart['id'];
		}
		return null;
	}

}

if (!function_exists('bab_OCEmptyOrgChart')) {

	function bab_OCEmptyOrgChart($orgChartId)
	{
		global $babDB;
	
		$sql = '
			SELECT id
			FROM ' . BAB_OC_ENTITIES_TBL . '
			WHERE
				id_oc = ' . $babDB->quote($orgChartId);
	
		$entities = $babDB->db_query($sql);
	
		$entityIds = array();
		while ($entity = $babDB->db_fetch_assoc($entities))
		{
			$entityIds[$entity['id']] = $entity['id'];
		}
	
		$sql = 'DELETE FROM ' . BAB_OC_ROLES_TBL . ' WHERE id_oc=' . $babDB->quote($orgChartId);
	
		$sql = '
			SELECT id
			FROM ' . BAB_OC_ROLES_TBL . '
			WHERE
				id_oc = ' . $babDB->quote($orgChartId);
	
		$roles = $babDB->db_query($sql);
	
		$roleIds = array();
		while ($role = $babDB->db_fetch_assoc($roles))
		{
			$roleIds[$role['id']] = $role['id'];
		}
	
	
		if (count($roleIds) > 0) {
			$sql = 'DELETE FROM ' . BAB_OC_ROLES_USERS_TBL . ' WHERE id_role IN(' . $babDB->quote($roleIds) .')';
			$babDB->db_query($sql);
		}
	
		$sql = 'DELETE FROM ' . BAB_OC_ROLES_TBL . ' WHERE id_oc=' . $babDB->quote($orgChartId);
		$babDB->db_query($sql);
	
		if (count($entityIds) > 0) {
			$sql = 'DELETE FROM ' . BAB_OC_ENTITIES_ENTITY_TYPES_TBL . ' WHERE id_entity IN(' . $babDB->quote($entityIds) .')';
			$babDB->db_query($sql);
		}
	
		$sql = 'DELETE FROM ' . BAB_OC_ENTITIES_TBL . ' WHERE id_oc=' . $babDB->quote($orgChartId);
		$babDB->db_query($sql);
		$sql = 'DELETE FROM ' . BAB_OC_ENTITY_TYPES_TBL . ' WHERE id_oc=' . $babDB->quote($orgChartId);
		$babDB->db_query($sql);
	
	
		$sql = 'DELETE FROM ' . BAB_OC_TREES_TBL . ' WHERE id_user=' . $babDB->quote($orgChartId);
		$babDB->db_query($sql);
	}
	
}