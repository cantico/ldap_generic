; <?php/*
[general]
name							="ldap_generic"
version							="0.1.10"
addon_type						="EXTENSION"
encoding						="UTF-8"
description						="Synchronize LDAP users"
description.fr					="Module permettant de synchroniser utilisateurs et groupes avec un annuaire LDAP ou Active Directory"
long_description.fr             ="README.md"
mysql_character_set_database	="latin1,utf8"
delete							=1
ov_version						="8.0.0"
php_version						="5.1.0"
addon_access_control			=0
author							="CANTICO"
icon							="48px-Icon-ldap.png"
mod_ldap						="Available"
configuration_page				="configure"
tags                            ="extension,users,admin,ldap"

[addons]

LibTimer						="0.4"

;*/?>
