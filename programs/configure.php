<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
bab_Widgets()->includePhpClass('Widget_Form');


class ovldap_OptionsEditor extends Widget_Form
{

	public function __construct()
	{
		$W = bab_Widgets();

		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(2,'em'));


		$this->setName('options');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();

		$this->setCanvasOptions($this->Options()->width(70,'em'));

		$this->addItems();
		$this->loadFormValues();

		$this->addButtons();
		$this->setSelfPageHiddenFields();
	}



	protected function addItems()
	{
		$W = bab_Widgets();
		
		
		$this->addItem($W->Link(ovldap_translate('Launch synchronization in a popup'), '?tg=addon/ldap_generic/ovldap-init')->setOpenMode(Widget_Link::OPEN_POPUP));
		
		
		$this->addItem($W->Section(ovldap_translate('Ldap settings'), $this->ldapsection())->setName('ldap'));

		$this->addItem($this->general());
		$this->addItem($this->users());
		$this->addItem($this->groups());
        $this->addItem($this->ldapgroups());
        $this->addItem($this->notifications());

		//$this->addItem($this->orgchart());
	}
	
	protected function general()
	{
	    $W = bab_Widgets();
	    
	    $general = $W->Section(ovldap_translate('General'), $W->VBoxLayout()->setVerticalSpacing(1.2,'em'))->setName('general');
	    
	    $general->addItem($timer = $this->boolfield('timer_active', ovldap_translate("Call synchronization with a daily timer")));
	    
	    $timerFrame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.8,'em'));
	    $timer->getInputWidget()->setAssociatedDisplayable($timerFrame, array(1));
	    $timerFrame->addItem($this->timerField('timer_hour', ovldap_translate("Process the daily synchronization at")));
	    
	    $general->addItem($timerFrame);
	    
	    $general->addItem($this->stringfield('logfile', ovldap_translate("Log file full path"), ovldap_translate('%s will be replaced by the database name')));
	    $general->addItem($disableusers = $this->boolfield('disableusers', ovldap_translate("Disable ovidentia users not present in LDAP server")));
	    
	    $disableFrame = $W->Frame(null, $W->VBoxLayout()->setVerticalSpacing(.8,'em'));
	    
	    $disableusers->getInputWidget()->setAssociatedDisplayable($disableFrame, array(1));
	    
	    $disableFrame->addItem($this->boolfield('neverdisableadmin', ovldap_translate("Never disable ovidentia administrators")));
	    $disableFrame->addItem($this->boolfield('neverdisableauth', ovldap_translate("Never disable users without LDAP authentication")));
	    
	    $general->addItem($disableFrame);
	    
	    $general->addItem($this->stringfield('defaultemail', ovldap_translate("Default email if no mail in ldap entry")));
	    
	    return $general;
	}
	
	protected function users()
	{
	    $W = bab_Widgets();
	    
	    $users = $W->Section(ovldap_translate('Users'), $W->VBoxLayout()->setVerticalSpacing(.8,'em'))->setName('users');
	    
	    $users->addItem($this->readonly('ovidentia_basedn', ovldap_translate("Use Base DN configured in ovidentia LDAP authentication")));
	    $users->addItem($this->readonly('ovidentia_mapping', ovldap_translate("Use field mapping configured on ovidentia LDAP authentication")));
	    $users->addItem($this->stringfield('filter', ovldap_translate("LDAP Filter used to retreive the users list")));
	    
	    return $users;
	}
	
	
	protected function groups()
	{
	    $W = bab_Widgets();
	     
	    $ldapgroups = $W->Section(ovldap_translate('Groups from a field'), $W->VBoxLayout()->setVerticalSpacing(.8,'em'))->setName('groups');
	     
	    $ldapgroups->addItem($this->stringfield('groupsattribute', ovldap_translate("User entry field name with the groups names")));
	    $ldapgroups->addItem($this->groupfield('root', ovldap_translate("Create groups under")));

	    return $ldapgroups;
	}
	
	
	protected function ldapgroups()
	{
	    $W = bab_Widgets();
	    
	    $ldapgroups = $W->Section(ovldap_translate('LDAP groups'), $W->VBoxLayout()->setVerticalSpacing(.8,'em'))->setName('ldapgroups');
	    
	    $ldapgroups->addItem($this->stringfield('userfield', ovldap_translate("User entry field name with the group list")));
	    $ldapgroups->addItem($this->groupfield('root', ovldap_translate("Common group in ovidentia and LDAP used as a root node for synchronization")));
	    $ldapgroups->addItem($this->boolfield('create_groups', ovldap_translate("Create groups in Ovidentia if not exists")));
	    $ldapgroups->addItem($this->boolfield('remove_from_group', ovldap_translate("Remove users from group if group not found in ldap group tree")));
	    $ldapgroups->addItem($this->boolfield('remove_from_groups_on_disable', ovldap_translate("If a user does not exists in LDAP, remove user from groups under the configured root group")));
	    $ldapgroups->addItem($this->group_path_method());
	     
	    
	    return $ldapgroups;
	}
	
	
	
	protected function notifications()
	{
	    $W = bab_Widgets();
	     
	    $notifications = $W->Section(ovldap_translate('Notifications'), $W->VBoxLayout()->setVerticalSpacing(.8,'em'))->setName('notifications');
	    
	    $notifications->addItem($this->stringfield('recipients', ovldap_translate("Recipients, coma separated emails list")));
	    
	    return $notifications;
	}
	
	
	protected function group_path_method()
	{
	    $W = bab_Widgets();
	    
	    
	    return $W->LabelledWidget(
	        ovldap_translate('Method used to get the group path'), 
	        $W->RadioSet()
	           ->addOption('dn', ovldap_translate('Use the full DN path'))
	           ->addOption('memberof', ovldap_translate('Use the memberof attribute')),
	        __FUNCTION__
	    );
	}
	
	
	protected function orgchart()
	{
	    $W = bab_Widgets();
	    $baseUrl = bab_url::get_request('tg');
	    $rebuildOrgchartUrl = $baseUrl->toString() . '&idx=build_orgchart';
	    
	    $rebuildOrgchartFromGroupUrl = $baseUrl->toString() . '&idx=build_orgchart_from_group';
	    
	    $orgchart = $W->Section(ovldap_translate('Organizational chart'), $W->VBoxLayout()->setVerticalSpacing(.8,'em'))->setName('orgchart');
	    
	    $orgchart->addItem($this->boolfield('rebuild_orgchart', ovldap_translate("Rebuild organizational chart after synchronization")));
	    $orgchart->addItem(
	        $W->Link(ovldap_translate("Build organizational chart now"), $rebuildOrgchartUrl)
	        ->setConfirmationMessage(ovldap_translate("This definitilely will erase the current organizational chart ! Continue ?"))
	    );
	    
	    $orgchart->addItem($this->groupfield('rebuild_orgchart_basegroup', ovldap_translate("Base group under which groups associated to entities will be created")));
	    $orgchart->addItem(
	        $W->Link(ovldap_translate("Build organizational chart from group"), $rebuildOrgchartFromGroupUrl)
	        ->setConfirmationMessage(ovldap_translate("This definitilely will erase the current organizational chart ! Continue ?"))
	    );
	    
	    $orgchart->addItem($this->stringfield('rebuild_orgchart_name', ovldap_translate("Name of imported organizational chart")));
	    
	    $orgchart->addItem($this->stringfield('entity_field', ovldap_translate("Directory field used to define the user's entity in the organizational chart")));
	    $orgchart->addItem($this->stringfield('parent_entity_field', ovldap_translate("Directory field used to define the parent entity of the user's entity in the organizational chart")));
	    $orgchart->addItem($this->stringfield('role_field', ovldap_translate("Directory field used to define the role of the user in the entity")));
	    
	    return $orgchart;
	}
	
	
	protected function ldapsection()
	{
	    $W = bab_Widgets();
	    $ovidentia_parameters = $W->CheckBox();
	    $checkbox = $W->LabelledWidget(ovldap_translate("Use Ovidentia ldap settings"), $ovidentia_parameters, 'ovidentia_parameters');
	    
	    $layout = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
	    
	    $layout->addItem($checkbox);
	    
	    $settings = $W->VBoxLayout()->setVerticalSpacing(1, 'em');
	    $layout->addItem($settings);
	    
	    $settings->addItem($W->HBoxItems(
	        $W->LabelledWidget(ovldap_translate("Host"), $W->LineEdit()->setSize(50), 'host'),
	        $W->LabelledWidget(ovldap_translate("Port"), $W->LineEdit()->setSize(6), 'port')
		)->setHorizontalSpacing(2,'em'));
	    $settings->addItem($this->stringfield('bindn', ovldap_translate("Bind name")));
	    $settings->addItem($this->stringfield('password', ovldap_translate("Password")));
	    
	    $ovidentia_parameters->setAssociatedDisplayable($settings, array('0'));
	    
	    return $layout;
	}

	
	protected function readonly($name, $title)
	{
		$W = bab_Widgets();
		return $W->LabelledWidget($title, $W->CheckBox()->setDisplayMode(), $name);
	}
	

	protected function stringfield($name, $title, $description = null)
	{
		$W = bab_Widgets();
		return $W->LabelledWidget($title, $W->LineEdit()->setSize(70), $name, $description);
	}
	
	/**
	 * 
	 * @param string $name
	 * @param string $title
	 * 
	 * @return Widget_LabelledWidget
	 */
	protected function boolfield($name, $title)
	{
		$W = bab_Widgets();
		return $W->LabelledWidget($title, $W->CheckBox(), $name);
	}


	protected function groupfield($name, $title)
	{
		$W = bab_Widgets();
		return $W->LabelledWidget($title, $W->GroupPicker(), $name);
	}
	
	
	protected function timerField($name, $title)
	{
		$W = bab_Widgets();
		
		$arr = array();
		
		for($h=0; $h<24; $h++)
		{
			$arr[$h] = sprintf('%02d:00', $h);
		}
		
		return $W->LabelledWidget($title, $W->Select()->setOptions($arr), $name);
	}


	protected function loadFormValues()
	{
		global $babDB;

		$values = ovldap_getConfiguration();
		bab_debug($values);
		
		$this->setValues($values, array('options'));
	}


	protected function addButtons()
	{
		$W = bab_Widgets();

		$button = $W->FlowItems(
				$W->SubmitButton()->setName('save')->setLabel(ovldap_translate('Save'))
		)->setSpacing(1,'em');

		$this->addItem($button);
	}
}






function ovldap_adminOptions()
{
	require_once $GLOBALS['babInstallPath'].'admin/acl.php';
	$W = bab_Widgets();
	$page = $W->BabPage();


	$idx = bab_rp('idx', null);
	if ($idx == 'build_orgchart')
	{
		require_once dirname(__FILE__).'/syncincl.php';
		$sync = new ovldap_Sync(null);
		$sync->rebuildOrgChart(true);
	}
	
	if ($idx == 'build_orgchart_from_group') {
		require_once dirname(__FILE__).'/syncincl.php';
		$sync = new ovldap_Sync(null);
		$sync->createOrgChartFromGroup();
	}
	

	if (isset($_POST['options']))
	{
		
		if( isset($_POST['options']['save'] ))
		{
			$values = $_POST['options'];
			$registry = bab_getRegistryInstance();
			
			
			
			foreach($values as $section => $arr)
			{
				if (is_array($arr))
				{
					$registry->changeDirectory("/ldap_generic/$section/");
					
					foreach($arr as $key => $value)
					{
						$registry->setKeyValue($key, $value);
					}
					
				
				}
			}
		}
		
		$url = bab_url::get_request('tg');
		$url->location();
	}



	$editor = new ovldap_OptionsEditor();

	$page->setTitle(ovldap_translate('Options'));
	$page->addItem($editor);
	$page->displayHtml();
}



bab_requireCredential();

if (!bab_isUserAdministrator()) {
    throw new Exception('Access denied');
}



ovldap_adminOptions();
